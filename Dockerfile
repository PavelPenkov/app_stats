FROM openjdk:8u111-jdk
RUN apt-get update
RUN apt-get install -y apt-transport-https
RUN echo "deb https://dl.bintray.com/sbt/debian /" > /etc/apt/sources.list.d/sbt.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
RUN apt-get update
RUN apt-get install sbt
RUN mkdir -p /app-stats
WORKDIR /app-stats
ADD build.sbt /app-stats
ADD project /app-stats/project
ENV DB_HOST=localhost DB_USER=root DB_PASSWORD=password DB_NAME=app_stats
RUN sbt update
ADD . /app-stats
RUN sbt stage
EXPOSE 9999
CMD ./target/universal/stage/bin/app-stats -Djava.net.preferIPv4Stack=true
