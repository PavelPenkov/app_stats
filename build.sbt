name := "app-stats"
organization := "me.penkov"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.11.8"

fork in run := true

javaOptions ++= Seq(
  "-Dlog.service.output=/dev/stdout",
  "-Dlog.access.output=/dev/stdout")

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  "Twitter Maven" at "https://maven.twttr.com")

enablePlugins(JavaAppPackaging)

flywayUrl := s"jdbc:mysql://${sys.env("DB_HOST")}:3306/app_stats"
flywayUser := sys.env("DB_USER")
flywayPassword := sys.env("DB_PASSWORD")

lazy val versions = new {
  val finatra = "2.5.0"
  val finagle = "6.39.0"
  val guice = "4.0"
  val logback = "1.1.7"
  val mockito = "1.9.5"
  val scalatest = "2.2.6"
  val specs2 = "2.3.12"
}

libraryDependencies ++= Seq(
  "com.twitter" %% "finatra-http" % versions.finatra,
  "com.twitter" %% "finagle-redis" % versions.finagle,
  "com.twitter" %% "finagle-mysql" % versions.finagle,
  "org.jsoup" % "jsoup" % "1.10.1",
  "commons-validator" % "commons-validator" % "1.5.1",
  "mysql" % "mysql-connector-java" % "5.1.40",
  "joda-time" % "joda-time" % "2.9.6",
  "ch.qos.logback" % "logback-classic" % versions.logback,
  "ch.qos.logback" % "logback-classic" % versions.logback % "test",

  "com.twitter" %% "finatra-http" % versions.finatra % "test",
  "com.twitter" %% "inject-server" % versions.finatra % "test",
  "com.twitter" %% "inject-app" % versions.finatra % "test",
  "com.twitter" %% "inject-core" % versions.finatra % "test",
  "com.twitter" %% "inject-modules" % versions.finatra % "test",
  "com.google.inject.extensions" % "guice-testlib" % versions.guice % "test",

  "com.twitter" %% "finatra-http" % versions.finatra % "test" classifier "tests",
  "com.twitter" %% "inject-server" % versions.finatra % "test" classifier "tests",
  "com.twitter" %% "inject-app" % versions.finatra % "test" classifier "tests",
  "com.twitter" %% "inject-core" % versions.finatra % "test" classifier "tests",
  "com.twitter" %% "inject-modules" % versions.finatra % "test" classifier "tests",

  "org.mockito" % "mockito-core" % versions.mockito % "test",
  "org.scalatest" %% "scalatest" % versions.scalatest % "test",
  "org.specs2" %% "specs2" % versions.specs2 % "test")
