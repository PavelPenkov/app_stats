package me.penkov


import org.joda.time.LocalDateTime
import com.google.inject.Provides
import com.twitter.inject.TwitterModule
import com.twitter.util.Future

class InmemDeviceRepository extends DeviceRepository {
  var devices = List.empty[Device]
  var nextId = 1
  override def create(imei: String): Future[Device] = {
    val device = Device(nextId, imei, new LocalDateTime())
    devices = device::devices
    nextId+=1
    Future.value(device)
  }

  def findByImei(imei: String): Future[Option[Device]] = Future.value(devices.find { d => d.imei == imei })
}

class InmemStatsEntriesRepository extends StatsEntryRepository {
  var statsEntries = List.empty[StatsEntry]

  override def create(deviceId: Long, appDetails: Seq[App]) = {
    val statsEntry = StatsEntry(deviceId, appDetails, LocalDateTime.now)
    statsEntries = statsEntry::statsEntries
    Future.value(statsEntry)
  }

  override def forImeiAndDate(imei: String, date: LocalDateTime) = {
    if (imei == "123456789012345")
      Future.value(None)
    else
      Future.value(Some(StatsEntry(1, List.empty[App], LocalDateTime.now)))
  }

  override def forImei(imei: String) = {
    if (imei == "123456789012345")
      Future.value(None)
    else
      Future.value(Some(StatsEntry(1, List.empty[App], LocalDateTime.now)))
  }
}

class InmemDb extends Db {
  override def devices: DeviceRepository = new InmemDeviceRepository

  override def statsEntries: StatsEntryRepository = new InmemStatsEntriesRepository
}

object AppStatsTestingModule extends TwitterModule {
  override def configure = {
    bind[Db].to[InmemDb]
  }
}
