package me.penkov

import com.twitter.util.Await
import org.scalatest.{FunSuite, Matchers}

class PlayCrawlerSuite extends FunSuite with Matchers {
  val crawler = new PlayCrawler
  val details = Await.result(crawler.details(Seq("com.yandex.browser"))).head.details

  test("gets correct rating") {
    details.rating should equal(4.5)
  }

  test("gets correct price") {
    details.price should equal("0")
  }

  test("gets correct installs") {
    details.installs should equal(10000000L)
  }
}
