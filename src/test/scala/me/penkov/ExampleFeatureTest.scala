package me.penkov

import com.google.inject.testing.fieldbinder.Bind
import com.twitter.finagle.http.Status._
import com.twitter.finatra.http.{EmbeddedHttpServer, HttpTest}
import com.twitter.inject.Mockito
import com.twitter.inject.server.FeatureTest
import com.twitter.util.Future
import org.joda.time.LocalDateTime

class AppFeatureTest extends FeatureTest with Mockito with HttpTest {
  val mockDeviceRepo = smartMock[DeviceRepository]
  mockDeviceRepo.create(any[String]) returns Future(Device(1, "111111111111", LocalDateTime.now))

  @Bind  val mockDb = smartMock[Db]
  mockDb.devices returns mockDeviceRepo
  override val server = new EmbeddedHttpServer(
    twitterServer = new AppStatsServer {
      override def overrideModules = Seq(AppStatsTestingModule)
    }
  )

  val imei = "470817225568763"

  "Server" should {
    "create device" in {
      val json =
        s"""
          |{
          |  "imei" : "$imei"
          |}
        """.stripMargin

      server.httpPost(
        path = "/devices",
        postBody = json,
        andExpect = Created)
    }

    "reject invalid IMEI" in {
      val json =
        s"""
           |{
           |  "imei" : "1234"
           |}
        """.stripMargin

      server.httpPost(
        path = "/devices",
        postBody = json,
        andExpect = BadRequest
      )
    }

    "submit app stats" in  {
      val json =
        """
          |{
          |  "package_names" : ["com.yandex.browser", "ru.yandex.taxi"]
          |}
        """.stripMargin

      server.httpPost(
        path = s"/devices/$imei/stats",
        postBody = json,
        andExpect = Created
      )
    }

    "get app stats" in {
      server.httpGet(
        path = s"/devices/$imei/stats?date=2016-11-25",
        andExpect = Ok
      )
    }
  }
}
