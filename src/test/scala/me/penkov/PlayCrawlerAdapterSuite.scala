package me.penkov

import com.twitter.util.Await
import org.scalatest.{FunSuite, Matchers}
import com.twitter.finagle.Redis
import com.twitter.finatra.json.FinatraObjectMapper

class PlayCrawlerAdapterSuite extends FunSuite with Matchers {
  val adapter = new PlayCrawlerAdapter(new PlayCrawler, Redis.newRichClient("localhost:6379"), FinatraObjectMapper.create(null))
  val details = Await.result(adapter.details(Seq("com.yandex.browser"))).head.details

  test("gets correct rating") {
    details.rating should equal(4.5)
  }

  test("gets correct price") {
    details.price should equal("0")
  }

  test("gets correct installs") {
    details.installs should equal(10000000L)
  }
}
