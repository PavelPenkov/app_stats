CREATE TABLE devices (
    id BIGINT NOT NULL AUTO_INCREMENT,
    imei VARCHAR(15) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX ix_devices_imei ON devices(imei);

CREATE TABLE stats_entries
(
  id BIGINT NOT NULL AUTO_INCREMENT,
  device_id BIGINT NOT NULL,
  apps TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (device_id) REFERENCES devices(id)
);

CREATE INDEX ix_stats_entries_created_at_device_id ON stats_entries (device_id, created_at DESC);
