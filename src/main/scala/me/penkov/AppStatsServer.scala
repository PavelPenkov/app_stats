package me.penkov

import com.google.inject.{Provides, Singleton}
import com.twitter.finagle._
import com.twitter.finagle.client.DefaultPool
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.{CommonFilters, LoggingMDCFilter, TraceIdMDCFilter}
import com.twitter.finatra.http.routing.HttpRouter
import com.twitter.inject.TwitterModule
import com.twitter.util.{Duration, Future}

class NonRestrictiveCorsFilter extends SimpleFilter[Request, Response] {
  def apply(request: Request, service: Service[Request, Response]): Future[Response] = {
    service(request) map { rep =>
      rep.headerMap.add("Access-Control-Allow-Origin", "*")
      rep.headerMap.add("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
      rep.headerMap.add("Access-Control-Allow-Headers", "Content-Type")
      rep.headerMap.add("Access-Control-MaxAge", "86400")
      rep
    }
  }
}

object AppStatsModule extends TwitterModule {
  @Provides @Singleton
  def redisClient: redis.Client = Redis.newRichClient(s"${sys.env("REDIS_HOST")}:6379")

  @Provides @Singleton
  def mysqlClient = {
    Mysql.client.withCredentials(sys.env("DB_USER"), sys.env("DB_PASSWORD"))
      .withDatabase(sys.env("DB_NAME"))
      .configured(DefaultPool.Param(0, 10, 0, Duration.fromSeconds(300), 0))
      .withRequestTimeout(Duration.fromSeconds(5))
      .newRichClient(s"${sys.env("DB_HOST")}:3306")
  }

  override def configure = {
    bind[AppInfo].to[PlayCrawlerAdapter]
    bind[Db].to[MysqlDb]
  }
}
object AppStatsServerMain extends AppStatsServer

class AppStatsServer extends HttpServer {
  override val modules = Seq(AppStatsModule)

  override def defaultFinatraHttpPort = ":9999"

  override def configureHttp(router: HttpRouter) {
    router
      .filter[LoggingMDCFilter[Request, Response]]
      .filter[TraceIdMDCFilter[Request, Response]]
      .filter(new NonRestrictiveCorsFilter())
      .filter[CommonFilters]
      .add[AppStatsController]
  }
}
