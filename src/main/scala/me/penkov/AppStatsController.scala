package me.penkov

import com.google.inject.Inject
import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller
import com.twitter.finatra.request.RouteParam
import com.twitter.finatra.validation.{MethodValidation, NotEmpty, ValidationResult}
import com.twitter.util.{Future, ConstFuture}
import org.apache.commons.validator.routines.checkdigit.LuhnCheckDigit
import org.joda.time.LocalDateTime

case class DeviceRequest(@NotEmpty imei: String) {
  @MethodValidation
  def validateImei = {
    ValidationResult.validate(
      imei.length == 15 && LuhnCheckDigit.LUHN_CHECK_DIGIT.isValid(imei),
      "invalid IMEI"
    )
  }
}

case class SubmitStatsRequest(@RouteParam imei: String, apps: Array[String])
case class GetStatsRequest(@RouteParam imei: String, date: Option[org.joda.time.LocalDateTime])

class AppStatsController @Inject() (db: Db, appInfo: AppInfo) extends Controller {
  post("/devices") { request: DeviceRequest =>
    db.devices.create(request.imei).map { device =>
      response.created.json(device)
    } rescue {
        case e: RecordNotUnique =>
          Future.value(response.badRequest.json(Map("errors" -> Seq("IMEI already registered")).asInstanceOf[Any]))
      }
  }

  get("/devices/:imei/stats") { request: GetStatsRequest =>
    request.date match {
      case Some(date) => db.statsEntries.forImeiAndDate(request.imei, date)
      case _ => db.statsEntries.forImei(request.imei)
    }
  }

  post("/devices/:imei/stats") { request: SubmitStatsRequest =>
    db.devices.findByImei(request.imei) flatMap {
      case Some(device) =>
        for {
          infos <- appInfo.details(request.apps)
          statsEntry <- db.statsEntries.create(device.id, infos)
        } yield response.created.json(statsEntry)
      case None => Future.value(response.notFound)
    }
  }

  get("/swagger_doc") { request: Request =>
    val rep = response.ok.file("swagger.json")
    rep.setContentType("application/json")
    rep
  }

  options("/:*") { req: Request =>
    response.ok
  }

  get("/:*") { req: Request =>
    response.ok.fileOrIndex(req.params("*"), "swagger_ui/index.html")
  }
}
