package me.penkov

import com.twitter.util.Future
import org.joda.time.LocalDateTime
import com.twitter.finagle.mysql._
import java.sql.Timestamp

import com.google.inject.Inject
import com.twitter.finatra.json.FinatraObjectMapper

case class Device(id: Long, imei: String, createdAt: LocalDateTime)
case class StatsEntry(id: Long, appDetails: Seq[App], createdAt: LocalDateTime)

class RecordNotUnique extends Exception

trait DeviceRepository {
  def create(imei: String): Future[Device]
  def findByImei(imei: String): Future[Option[Device]]
}

trait StatsEntryRepository {
  def create(deviceId: Long, apps: Seq[App]) : Future[StatsEntry]
  def forImeiAndDate(imei: String, date: LocalDateTime): Future[Option[StatsEntry]]
  def forImei(imei: String): Future[Option[StatsEntry]]
}

trait Db {
  def devices: DeviceRepository
  def statsEntries: StatsEntryRepository
}

class MysqlDeviceRepository(client: Client)  extends DeviceRepository {
  def create(imei: String): Future[Device] = {
    val createdAt = new LocalDateTime()
    val query = s"INSERT INTO devices (imei, created_at) VALUES (?, ?)"
    val stmt = client.prepare(query)
    stmt(imei, new Timestamp(createdAt.toDateTime.getMillis)) map { case r: OK => Device(r.insertId.toInt, imei, createdAt) } rescue {
      case e: ServerError if e.code == 1062 && e.sqlState == "#23000" => throw new RecordNotUnique()
    }
  }

  def findByImei(imei: String): Future[Option[Device]] = {
    val query = "SELECT * FROM devices WHERE imei = ? LIMIT 1"
    val stmt = client.prepare(query)
    stmt.select(imei)(extract) map (_.headOption)
  }

  private def extract(row: Row) = {
    val id = row("id") match {
      case Some(LongValue(i)) => i
      case _ => throw new IllegalArgumentException("can't parse id")
    }

    val imei = row("imei") match {
      case Some(StringValue(s)) => s
      case _ => throw new IllegalArgumentException("can't parse imei")
    }

    val createdAt = row("created_at") match {
      case Some(TimestampValue(ts)) => new LocalDateTime(ts.getTime)
      case _ => throw new IllegalArgumentException("can't parse created_at")
    }

    Device(id, imei, createdAt)
  }
}

class MysqlStatsEntryRepository(client: Client) extends StatsEntryRepository {
  def forImeiAndDate(imei: String, date: LocalDateTime): Future[Option[StatsEntry]] = {
    val query = """
    SELECT stats_entries.*
    FROM stats_entries se
    JOIN devices d on se.device_id = device.id
    WHERE d.imei = ? AND se.created_at < ?
    ORDER BY se.created_at DESC
    LIMIT 1
    """
    val stmt = client.prepare(query)
    stmt.select(imei, new Timestamp(date.toDateTime.getMillis))(extract) map (_.headOption)
  }

  def create(deviceId: Long, apps: Seq[App]): Future[StatsEntry] = {
    val mapper = FinatraObjectMapper.create(null)
    val appsJson = mapper.writeValueAsString(apps)
    val createdAt = new LocalDateTime()
    val query = "INSERT INTO stats_entries (device_id, apps, created_at) VALUES (?, ?, ?)"
    val stmt = client.prepare(query)
    stmt(deviceId, appsJson, new Timestamp(createdAt.toDateTime.getMillis)) map { case r: OK => StatsEntry(r.insertId, apps, createdAt)}
  }

  def forImei(imei: String): Future[Option[StatsEntry]] = {
    val query = """
    SELECT se.*
    FROM stats_entries se
    JOIN devices d on se.device_id = d.id
    WHERE d.imei = ?
    ORDER BY se.created_at DESC
    LIMIT 1
    """
    val stmt = client.prepare(query)
    stmt.select(imei)(extract) map (_.headOption)
  }

  private def extract(row: Row) = {
    val id = row("id") match {
      case Some(LongValue(i)) => i
      case _ => throw new IllegalArgumentException("can't parse id")
    }

    val apps = row("apps") match {
      case Some(StringValue(s)) =>
        val mapper = FinatraObjectMapper.create(null)
        mapper.parse[Seq[App]](s)
      case _ => throw new IllegalArgumentException("can't parse apps")
    }

    val createdAt = row("created_at") match {
      case Some(TimestampValue(ts)) => new LocalDateTime(ts.getTime)
      case _ => throw new IllegalArgumentException("can't parse created_at")
    }
    StatsEntry(id, apps, createdAt)
  }
}

class MysqlDb @Inject() (client: Client) extends Db {
  val devices: DeviceRepository = new MysqlDeviceRepository(client)
  val statsEntries: StatsEntryRepository = new MysqlStatsEntryRepository(client)
}
