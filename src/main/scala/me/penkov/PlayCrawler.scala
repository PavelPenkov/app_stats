package me.penkov

import java.io.InputStream

import com.google.inject.Inject
import com.twitter.finagle.{Http, http, redis}
import com.twitter.finagle.redis.util.StringToBuf
import com.twitter.finatra.json.FinatraObjectMapper
import com.twitter.util.Future
import org.jsoup.Jsoup

import scala.collection.JavaConverters._

case class Details(price: String, rating: Double, installs: Long)
case class App(packageName: String, details: Details)

trait AppInfo {
  def details(packageNames: Seq[String]): Future[Seq[App]]
}

object PlayCrawler {
  def parse(is: InputStream) = {
    val doc = Jsoup.parse(is, "UTF-8", "https://play.google.com")
    val meta = doc.select("div.meta-info")
    val installs = {
      val info = meta.asScala.find { e =>
        e.select(".title").text().trim() == "Installs"
      }.get
      val range = info.select(".content").first.text.trim
      range.split(" - ")(0).replace(",", "").toLong
    }

    val score = doc.select(".score-container").first()
    val rating = score.select(".score").first.text.trim.toDouble

    val price = doc.select("meta[itemprop=\"price\"]").attr("content")

    Details(price, rating, installs)
  }
}

class PlayCrawler {
  val host = "play.google.com"
  val client = Http.client
    .withSessionPool.minSize(10)
    .withSessionPool.maxSize(100)
    .withSessionQualifier.noFailureAccrual
    .withTls(host)
    .newService(s"$host:443")

  def details(packageNames: Seq[String]) = {
    val tasks = packageNames.map { name =>
      client(detailsRequest(name)).map { resp =>
        resp.withInputStream { is =>
          App(name, PlayCrawler.parse(is))
        }
      }
    }
    Future.collect(tasks)
  }

  private def detailsRequest(packageName: String) = {
   http.Request("/store/apps/details", ("id", packageName), ("hl", "en"), ("gl", "us"))
  }
}

class PlayCrawlerAdapter @Inject() (crawler: PlayCrawler, cache: redis.Client, mapper: FinatraObjectMapper) extends AppInfo {
  val blackList = Seq(
    "foo.bar",
    "com.acme"
  )

  private def key(packageName: String) = StringToBuf(s"app/$packageName")

  override def details(packageNames: Seq[String]): Future[Seq[App]]= {
    val tasks = filter(packageNames) map { name =>
      getCachedApp(name) flatMap {
        case Some(app) => Future.value(app)
        case None => getAndCacheApp(name)
      }
    }
    Future.collect(tasks)
  }

  private def getAndCacheApp(packageName: String): Future[App] = {
    for {
      details <- crawler.details(Seq(packageName)).map(_.head)
      _ <- cache.set(key(packageName), mapper.writeValueAsBuf(details))
    } yield details
  }

  private def getCachedApp(packageName: String): Future[Option[App]] = {
    cache.get(key(packageName)) map {
      case Some(buf) => Some(mapper.parse[App](buf))
      case _ => None
    }
  }

  private def filter(packageNames: Seq[String]) = packageNames.filterNot { name =>
    blackList.exists(s => name.contains(s))
  }
}
