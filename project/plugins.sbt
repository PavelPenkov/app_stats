addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")
addSbtPlugin("org.flywaydb" % "flyway-sbt" % "4.0.3")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.5")

resolvers += "Flyway" at "https://flywaydb.org/repo"
